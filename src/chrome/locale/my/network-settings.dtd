<!ENTITY torsettings.dialog.title "Tor ကွန်ယက် အပြင်အဆင်များ">
<!ENTITY torsettings.wizard.title.default "Tor နှင့် ချိတ်ဆက်မယ်">
<!ENTITY torsettings.wizard.title.configure "Tor ကွန်ယက် အပြင်အဆင်များ">
<!ENTITY torsettings.wizard.title.connecting "ချိတ်ဆက်မှုလိုင်း တည်ထောင်နေသည်">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Tor ဘရောင်ဇာ ဘာသာစကား">
<!ENTITY torlauncher.localePicker.prompt "ကျေးဇူးပြု၍ ဘာသာစကားရွေးပါ">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Tor နှင့် ချိတ်ဆက်ရန် &quot;ချိတ်ဆက်မယ်&quot; ဆိုသည့် ခလုတ်အား နှိပ်ပါ">
<!ENTITY torSettings.configurePrompt "ကွန်ယက် အပြင်အဆင်များ ချိန်ညှိရန် &quot;စီစဥ်မယ်&quot; ဆိုသည့်ခလုတ်အား နှိပ်ပါ">
<!ENTITY torSettings.configure "စီစဥ်မယ်">
<!ENTITY torSettings.connect "ချိတ်ဆက်မယ်">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Tor စဖို့ စောင့်နေပါသည်...">
<!ENTITY torsettings.restartTor "Tor ကို ပြန်လည်စတင်မယ်">
<!ENTITY torsettings.reconfigTor "ပြန်ချိန်ညှိမယ်">

<!ENTITY torsettings.discardSettings.prompt "Tor ချိတ်ဆက်တံတားများအား သင်စီမံထားရှိပြီပါပြီ သို့မဟုတ် လိုကယ်ကြားခံ proxy များ၏ အပြင်အဆင်များကို ရိုက်ထည့်ပြီးပါပြီ။ ​&#160; Tor ကွန်ယက်နှင့် တိုက်ရိုက်ချိတ်ဆက်ရန် ၎င်းအပြင်အဆင်များကို ဖယ်ရှားရပါမည်။">
<!ENTITY torsettings.discardSettings.proceed "အပြင်မဆင်များ ဖယ်ရှားပြီး ချိတ်ဆက်မယ်">

<!ENTITY torsettings.optional "မဖြစ်မနေမဟုတ်">

<!ENTITY torsettings.useProxy.checkbox "ကျွန်ုပ်သည် ကြားခံ proxy သုံး၍ အင်တာနက်နှင့် ချိတ်ဆက်ပါသည်">
<!ENTITY torsettings.useProxy.type "ကြားခံ proxy အမျိုးအစား">
<!ENTITY torsettings.useProxy.type.placeholder "ကြားခံ proxy အမျိုးအစားတစ်ခု ရွေးပါ">
<!ENTITY torsettings.useProxy.address "လိပ်စာ">
<!ENTITY torsettings.useProxy.address.placeholder "IP လိပ်စာ သို့မဟုတ် ဧည့်ခံသူအမည် (hostname)">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "အသုံးပြုသူအမည်">
<!ENTITY torsettings.useProxy.password "စကားဝှက်">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "အချို့သော port များနှင့်သာ ချိတ်ဆက်မှုများခွင့်ပြုသည့် အသွားအလာထိန်းချုပ်သည့်အလွှာ (firewall) အား ဤကွန်ပျူတာသည် ဖြတ်သန်းပါသည်။">
<!ENTITY torsettings.firewall.allowedPorts "ခွင့်ပြုထားသော ports">
<!ENTITY torsettings.useBridges.checkbox "ကျွန်ုပ်၏ နိုင်ငံတွင် Tor ကို ဆင်ဆာခံထားပါသည်">
<!ENTITY torsettings.useBridges.default "ပင်ကိုသွင်းထားသော ချိတ်ဆက်တံတား ရွေးပါ">
<!ENTITY torsettings.useBridges.default.placeholder "ချိတ်ဆက်တံတား ရွေးပါ">
<!ENTITY torsettings.useBridges.bridgeDB "torproject.org မှ ချိတ်ဆက်တံတား တောင်းဆိုပါ">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "ရုပ်ပုံထဲမှ စာလုံးများကို ရိုက်ထည့်ပါ">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "စိန်ခေါ်မှုအသစ် ယူမယ်">
<!ENTITY torsettings.useBridges.captchaSubmit "တင်သွင်းပါ။">
<!ENTITY torsettings.useBridges.custom "ကျွန်ုပ်သိသော ချိတ်ဆက်တံတား ပံ့ပိုးပါ">
<!ENTITY torsettings.useBridges.label "ယုံကြည်ရသော အရင်းအမြစ်မှ ချိတ်ဆက်တံတား အချက်အလက် ရိုက်ထည့်ပါ">
<!ENTITY torsettings.useBridges.placeholder "လိပ်စာအမျိုးအစား : port (တစ်ကြောင်းတစ်ခုစီ)">

<!ENTITY torsettings.copyLog "Tor မှတ်တမ်းကို စာဘုတ်ပေါ်သို့ ကော်ပီကူးယူမယ်">

<!ENTITY torsettings.proxyHelpTitle "Proxy ကြားခံ အကူအညီ">
<!ENTITY torsettings.proxyHelp1 "ကုမ္ပဏီ၊ ကျောင်း၊ သို့မဟုတ် တက္ကသိုလ် ကွန်ယက်များနှင့် ချိတ်ဆက်လျှင် လိုကယ် ကြားခံ proxy လိုအပ်နိုင်ပါသည်။ &#160; သင်မှ ကြားခံ proxy လိုမလိုခြင်း မသေချာပါက အခြား ဘရောင်ဇာမှ အင်တာနက် အပြင်အဆင်များ သို့မဟုတ် သင့်စနစ်၏ ကွန်ယက်ချိတ်ဆက်မှု အပြင်အဆင်များကို လေ့လာကြည့်ရှုနိုင်ပါသည်။">

<!ENTITY torsettings.bridgeHelpTitle "ချိတ်ဆက်တံတား လက်ဆင့်ကမ်းခြင်း အကူအညီ">
<!ENTITY torsettings.bridgeHelp1 "ချိန်ဆက်တံတားများ သည် Tor ကွန်ယက်ချိတ်ဆက်မှုများ ကို ပိတ်ပယ်ရန် ခက်ခဲအောင်လုပ်သည့် စာရင်းမသွင်းထားသော လက်ဆင့်ကမ်းမှုများ ဖြစ်ပါသည်။ .&#160; ဆင်ဆာခံရခြင်းကို ရှောင်ကျင်ရန် ချိတ်ဆက်တံတား အမျိုးအစားများတစ်ခုစီသည် ကွဲပြားသော နည်းလမ်းများ အသုံးပြုပါသည်။ .&#160; Obfs အမျိုးအစားသည် သင့် အင်တာနက် အသွားအလာများကို ကျပန်းဆူညံသံကဲ့သို့ အယောင်ဆောင်ပြုပါသည်၊ Meeks အမျိုးအစားသည် သင့် အင်တာနက် အသွားအလာများ သို့မဟုတ် Tor နှင့် ချိတ်ဆက်ထားခြင်းကို အခြားဝန်ဆောင်မှုနှင့် ချိတ်ဆက်ထားခြင်းကဲ့သို့ အယောင်ဆောင်ပြုပါသည်။">
<!ENTITY torsettings.bridgeHelp2 "အချို့သောနိုင်ငံများတွင် Tor ကို ပိတ်ပယ်ရန် ကြိုးစားခြင်းကြောင့် အချို့သော ချိတ်ဆက်တံတားများသည် အချို့နိုင်ငံတွင်သာ အလုပ်လုပ်ပြီး အခြားနိုင်ငံများတွင် အလုပ်မလုပ်ပါ။ ​&#160; သင့်နိုင်ငံတွင် မည်သည့် ချိတ်ဆက်တံတားများက အလုပ်လုပ်သည်ကို မသေချာလျှင် torproject.org/about/contact.html သို့ ဝင်ရောက်ကြည့်ရှုပေးပါ။ #support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "ကျွန်ုပ်တို့မှ​ Tor ကွန်ယက်နှင့် ချိတ်ဆက်မှု ထူထောင်နေတုန်း ခေတ္တစောင့်ပေးပါ။ &#160; ဤလုပ်ဆောင်မှုသည် မိနစ်အနည်းအငယ်ကြာနိုင်လိမ်းမည်။">

<!-- #31286 about:preferences strings  -->
<!ENTITY torPreferences.categoryTitle "Tor">
<!ENTITY torPreferences.torSettings "Tor အပြင်အဆင်များ">
<!ENTITY torPreferences.torSettingsDescription "ကမ္ဘာအနှံ့ရှိ လုပ်အားပေးများ လုပ်ဆောင်သော Tor ကွန်ယက်ပေါ်တွင် သင့်အသွားအလာများကို Tor ဘရောင်ဇာမှ လမ်းကြောင်းပေးပါသည်။" >
<!ENTITY torPreferences.learnMore "ထပ်မံလေ့လာမယ်">
<!ENTITY torPreferences.bridges "Bridges">
<!ENTITY torPreferences.bridgesDescription "ချိတ်ဆက်တံတားများသည် Tor ကို ပိတ်ပယ်ထားသော နေရာများတွင် Tor ကွန်ယက်နှင့် ချိတ်ဆက်ရန် ကူညီပေးပါသည်။ သင့် တည်နေရာပေါ်မူတည်၍ ချိတ်ဆက်တံတားတစ်ခုသည် နောက်တစ်ခုထက် ပိုကောင်းနိုင်ပါလိမ့်မည်။">
<!ENTITY torPreferences.useBridge "ချိတ်ဆက်တံတား အသုံးပြုမယ်">
<!ENTITY torPreferences.requestNewBridge "ချိတ်ဆက်တံတားအသစ် တောင်းဆိုပါ...">
<!ENTITY torPreferences.provideBridge "ချိတ်ဆက်တံတားတစ်ခု ပံ့ပိုးမယ်">
<!ENTITY torPreferences.advanced "အဆင့်မြင့်">
<!ENTITY torPreferences.advancedDescription "Tor ဘရောင်ဇာမှ အင်တာနက်နှင့် မည်သို့ချိတ်ရန်ကို ချိန်ညှိပါရန်">
<!ENTITY torPreferences.firewallPortsPlaceholder "ကော်မာခြားထားသော တန်ဖိုးများ">
<!ENTITY torPreferences.requestBridgeDialogTitle "ချိတ်ဆက်တံတား တောင်းဆိုမယ်">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "BridgeDB နှင့် ဆက်သွယ်နေသည်။ ခေတ္တစောင့်ပေးပါ။">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "လူဟုတ်မဟုတ်စစ်ဆေးမှုကို ဖြေ၍ bridge အား တောင်းဆိုပါ">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "ဖြေရှင်းမှုသည် မမှန်ကန်ပါ။ ပြန်စမ်းကြည့်ပေးပါ။">
<!ENTITY torPreferences.viewTorLogs "Tor မှတ်တမ်းများ ကိုကြည့်ရှုမယ်">
<!ENTITY torPreferences.viewLogs "မှတ်တမ်းများ ကြည့်ရှုမယ်...">
<!ENTITY torPreferences.torLogsDialogTitle "Tor မှတ်တမ်းများ">
